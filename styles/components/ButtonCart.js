import styled from '@emotion/styled'
import { motion } from 'framer-motion'
import Typography from '@material-ui/core/Typography'

export const ButtonContainer = styled.button`
  background: ${(props) => props.theme[props.store].color};
  padding: 10px 20px 10px 20px;
  display: grid;
  grid-template-areas: 'label icon';
  grid-template-columns: 1fr 20px;
  cursor: pointer;
  overflow: hidden;
  user-select: none;
  border-style: none;
  margin-top: 18px;
  width: 100%;

  :focus {
    outline: none;
  }
`

export const Label = styled(Typography)`
  align-self: center;
  text-align: left;
  grid-area: label;
  font-family: Montserrat;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  margin-right: 10px;
`

export const IconWrapper = styled(motion.div)`
  grid-area: icon;
  align-self: center;
`
