import styled from '@emotion/styled'

export const LayoutContent = styled.main`
  padding: 6em;

  @media screen and (max-width: 500px) {
    padding: 2em;
    margin-top: 3em;
  }
`

export const LayoutFooter = styled.footer`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

export const LinkStyled = styled.a`
  margin: 15px;
  cursor: pointer;
  text-decoration: none;
  color: #000;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    padding-left: 10px;
  }
`
