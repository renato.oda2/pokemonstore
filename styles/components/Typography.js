import TypographyMaterial from '@material-ui/core/Typography'
import styled from '@emotion/styled'

export const TypographyStyled = styled(TypographyMaterial)`
  text-transform: ${(props) => props.transform};
`
