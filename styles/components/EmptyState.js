import styled from '@emotion/styled'

export const Container = styled.div``

export const Content = styled.section`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
  background-color: transparent;
  display: flex;
  flex-shrink: 1;
  flex-direction: column;
`

export const IconContainer = styled.div`
  display: 'block';
`
