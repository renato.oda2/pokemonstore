import styled from '@emotion/styled'
import Typography from '@material-ui/core/Typography'

export const Container = styled.div`
  box-shadow: 1px 5px 15px #ccc;
  border-radius: 3px;
  height: 100%;

  :hover {
    transform: scale(1.1, 1.1);
  }

  :hover:after {
    opacity: 1;
  }

  :active {
    transform: scale(1.1, 1.1);
  }
`

export const Content = styled.button`
  display: flex;
  align-items: center;
  flex-flow: column wrap;
  cursor: pointer;
  outline: none;
  border: none;
  width: 100%;

  background: linear-gradient(
    -45deg,
    ${(props) => props.theme[props.store].color} 50%,
    rgba(255, 255, 255, 0.5) 50%
  );
`

export const ProductImage = styled.img`
  max-width: 100%;
  text-align: center;
`
export const ProductCheckout = styled.div`
  display: flex;
  flex-direction: column;
  padding: 18px;
`

export const TextProduct = styled(Typography)`
  line-height: 1.66 !important;
  letter-spacing: 0.03333em !important;
  font-size: 1.65rem !important;
  text-transform: uppercase !important;
`

export const TextPriceProduct = styled(Typography)`
  font-weight: 100 !important;
  font-size: 1.25rem !important;
`
