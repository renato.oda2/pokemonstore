import styled from '@emotion/styled'
import Dialog from '@material-ui/core/Dialog'
import Grid from '@material-ui/core/Grid'

export const DialogCheckout = styled(Dialog)`
  .MuiDialog-scrollBody:after {
    padding-right: 0px;
  }

  .MuiDialog-paper {
    width: 40%;
    background-color: #ffffff;
    position: absolute;
    top: 0px;
    right: 0px;
    margin: 0px;
    border-radius: 0px;
    max-width: 100%;
    height: 100%;
    max-height: 100%;
    box-sizing: border-box;
    box-shadow: 0px 3px 22px rgba(0, 0, 0, 0.2);
    display: flex;
    flex-direction: row-reverse;
    padding: 0px;

    @media (max-width: 960px) {
      width: 100%;
    }
  }
`

export const CartCheckoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  height: 100%;
`

export const CartCheckoutContentStyled = styled.section`
  width: 100%;
  box-sizing: border-box;
  padding: 16px;
  flex: 1;
  height: 100%;
  overflow: auto;
`

export const CartCheckoutHeaderStyled = styled.header`
  width: 100%;
  height: 50px;
  min-height: 50px;
  align-items: center;
  display: flex;
  padding: 0px 15px;
  border-bottom: 1px solid #eaeaea;
  box-sizing: border-box;
  position: relative;
  background-color: white;
  align-items: center;
`

export const CartCheckoutButtonClose = styled.button`
  background-color: transparent;
  border-color: transparent;
  outline: none;
  cursor: pointer;

  :active {
    background-color: #f4f4f4;
  }
`
export const CartCheckoutFooterStyled = styled(Grid)`
  height: 50px;
  minheight: 50px;
  border-top: 1px solid #eaeaea;
  padding-left: 1rem;
`

export const CartCheckoutFooterCompleted = styled(Grid)`
  padding: 0 1rem;
`

export const CartButtonCheckout = styled.button`
  background: ${(props) => props.theme[props.store].color};
  padding: 10px 20px 10px 20px;
  display: grid;
  grid-template-areas: 'label icon';
  grid-template-columns: 1fr 20px;
  cursor: pointer;
  overflow: hidden;
  user-select: none;
  border-style: none;
  font-color: black;

  :focus {
    outline: none;
  }
`
export const CheckoutItemImage = styled.img`
  width: 150px;
  height: 150px;

  @media screen and (max-width: 500px) {
    width: 80px;
    height: 80px;
  }
`

export const CheckoutItemContent = styled.ul`
  list-style: none;
  padding: 0px;
  margin: 0px;
`

export const CheckoutItem = styled.li`
  width: 100%;
  border-bottom: 1px solid #c7c6c6;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;

  :hover {
    background-color: ${(props) => props.theme[props.store].color};
  }

  :active {
    background-color: ${(props) => props.theme[props.store].color};
  }
`
export const CheckoutItemProductDetails = styled.div`
  min-width: 40%;
`

export const CheckoutItemProductTotalValue = styled.div`
  width: 20%;
  justify-content: flex-start;
  display: flex;
`

export const CheckoutItemProductRemove = styled.button`
  background-color: transparent;
  border-color: transparent;
  outline: none;
  cursor: pointer;
`

export const ButtonCheckout = styled.button`
  background-color: transparent;
  border-color: transparent;
  outline: none;
  cursor: pointer;
`
