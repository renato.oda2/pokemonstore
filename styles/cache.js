import createCache from '@emotion/cache'

export const cache = createCache({ key: 'pokemon-store', prepend: true })
