import styled from '@emotion/styled'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`

export const ContainerTitle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 25vh;
  background-color: ${(props) => props.backgroundColor};
  cursor: pointer;
`
