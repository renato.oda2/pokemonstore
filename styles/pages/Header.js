import styled from '@emotion/styled'
import AppBar from '@material-ui/core/AppBar'
import Badge from '@material-ui/core/Badge'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'

export const HeaderAppBar = styled(AppBar)`
  background-color: ${(props) => props.theme[props.store].color} !important;
`

export const StoreIcon = styled.img`
  padding-right: 15px;
  cursor: pointer;
`

export const StoreNameContainer = styled.div`
  flex-grow: 1;
  display: 'none';
  cursor: auto;

  @media only screen and (min-width: 600px) {
    display: block;
  }
`

export const BadgeStyled = styled(Badge)`
  cursor: pointer;
`

export const SearchContainer = styled.div`
  width: 20%;
  padding-right: 20px;
`

export const SearchIconStyled = styled(SearchIcon)`
  height: 100%;
  position: absolute;
  pointerevents: none;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const InputStyled = styled(InputBase)`
  background-color: #fff;
  padding: 8px;
  width: 100% !important;
`
