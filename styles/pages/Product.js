import styled from '@emotion/styled'

export const Container = styled.div`
  height: 100%;
  padding-top: 5%;
  display: flex;
  flex-direction: center;
  align-items: center;
`

export const ContainerWrapper = styled.div`
  box-shadow: -45px 49px 83px 1px rgba(0, 0, 0, 0.45);
  display: block;
  margin: 20px auto;
  width: auto;
  position: relative;
  background-color: white;
  padding: 50px;
`
export const ProductDetails = styled.div`
  max-width: 600px;
`
export const ProductLeft = styled.section`
  display: inline-block;
  padding-right: 4%;
  vertical-align: top;
  width: 46%;

  @media (max-width: 650px) {
    width: 100%;
    padding-right: 0px;
  }
`
export const ProductImageContainer = styled.div`
  padding: 10px 10px 0px 10px;

  img {
    object-fit: contain;
  }
`
export const ProductRight = styled.aside`
  display: inline-block;
  vertical-align: top;
  width: 49%;

  @media (max-width: 650px) {
    width: 100%;
  }
`
export const ProductFooter = styled.footer`
  border-top: 1px solid #ccc;
  position: relative;
  padding: 22px 0px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const ButtonGoBack = styled.button`
  position: absolute;
  left: 10px;
  top: 10px;
  color: #222;
  width: 0;
  height: 0;
  border-top: 60px solid ${(props) => props.theme[props.store].color};
  border-right: 60px solid transparent;
  background-color: transparent;
  cursor: pointer;
  border-bottom: transparent;
  border-left: transparent;

  :focus {
    outline: none;
  }

  svg {
    font-size: 20px;
    position: relative;
    top: -52px;
    left: 4px;
  }
`
export const ProductInfo = styled.div`
  display: inline-block;
  width: 60%;
  vertical-align: top;
  @media (max-width: 512px) {
    width: 100%;
  }
`
