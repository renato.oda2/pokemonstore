export const theme = {
  dragon: {
    color: '#F9BE00',
  },
  water: {
    color: '#609FB5',
  },
  fire: {
    color: '#FB6C6C',
  },
  psychic: {
    color: '#9B7FA6',
  },
  default: {
    color: '#C2C2A1',
  },
}
