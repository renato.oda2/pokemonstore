'use strict'

const configBabel = () => {
  const plugins = ['@emotion/babel-plugin']

  if (process.env.CYPRESS) {
    plugins.push('istanbul')
  }

  return {
    presets: [
      [
        'next/babel',
        {
          'preset-react': {
            runtime: 'automatic',
            importSource: '@emotion/react',
          },
        },
      ],
    ],
    plugins: plugins,
  }
}

module.exports = configBabel()

