export const allStores = [
  {
    id: 'dragon',
    label: 'Dragon Store',
    uri: '/dragon',
    logo: '/assets/images/dragon.svg',
    background: '#F9BE00',
  },
  {
    id: 'water',
    label: 'Water Store',
    uri: '/water',
    logo: '/assets/images/water.svg',
    background: '#609FB5',
  },
  {
    id: 'fire',
    label: 'Fire Store',
    uri: '/fire',
    logo: '/assets/images/fire.svg',
    background: '#FB6C6C',
  },
  {
    id: 'psychic',
    label: 'Psychic Store',
    uri: '/psychic',
    logo: '/assets/images/psychic.svg',
    background: '#9B7FA6',
  },
]

export const getStore = (store) => {
  switch (store) {
    case 'dragon':
      return {
        id: 'dragon',
        label: 'Dragon Store',
        uri: '/dragon',
        logo: '/assets/images/dragon.svg',
      }
    case 'water':
      return {
        id: 'water',
        label: 'Water Store',
        uri: '/water',
        logo: '/assets/images/water.svg',
      }
    case 'fire':
      return {
        id: 'fire',
        label: 'Fire Store',
        uri: '/fire',
        logo: '/assets/images/fire.svg',
      }
    case 'psychic':
      return {
        id: 'psychic',
        label: 'Psychic Store',
        uri: '/psychic',
        logo: '/assets/images/psychic.svg',
      }
    default:
      return {
        id: 'default',
        label: `${store} Store`,
        uri: `/${store}`,
        logo: '/assets/images/default.svg',
      }
  }
}
