const BASE_URL = 'https://pokeapi.co/api/v2'

export const fetchPokemonByType = async (type) => {
  try {
    const res = await fetch(`${BASE_URL}/type/${type}?limit=20&offset=0`)

    if (res.status !== 200) {
      return { status: res.status }
    }

    const data = await res.json()
    return {
      data: data.pokemon,
      status: res.status,
    }
  } catch (error) {
    return { error }
  }
}

export const fetchPokemonByUrl = async (url) => {
  try {
    const res = await fetch(url)

    if (res.status !== 200) {
      return { status: res.status }
    }

    const data = await res.json()
    return {
      data: {
        id: data.id,
        pokedex: data.id.toString().padStart(3, '000'),
        price: Number(data.base_experience / 2).toLocaleString('pt-BR', {
          currency: 'BRL',
          minimumFractionDigits: 2,
        }),
        sprites: data.sprites,
      },
      status: res.status,
    }
  } catch (error) {
    return { error }
  }
}

export const fetchPokemonById = async (id) => {
  try {
    const res = await fetch(`${BASE_URL}/pokemon/${id}?limit=20&offset=0`)

    if (res.status !== 200) {
      return { status: res.status }
    }

    const data = await res.json()

    return {
      data: {
        id: data.id,
        name: data.name,
        pokedex: data.id.toString().padStart(3, '000'),
        price: Number(data.base_experience / 2).toLocaleString('pt-BR', {
          currency: 'BRL',
          minimumFractionDigits: 2,
        }),
        stats: data.stats,
        sprites: data.sprites,
      },
      status: res.status,
    }
  } catch (error) {
    return { error }
  }
}
