import { useMemo } from 'react'
import Link from 'next/link'
import Toolbar from '@material-ui/core/Toolbar'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

import {
  HeaderAppBar,
  BadgeStyled,
  StoreIcon,
  SearchContainer,
  StoreNameContainer,
  InputStyled,
} from '../../../styles/pages/Header'
import { useCart } from '../../hooks/Cart'
import { useFilter } from '../../hooks/Filter'
import Typography from '../../components/Typography'

export default function Header({ store }) {
  const { cart, handleOpenCart, productCountStore } = useCart()
  const filter = useFilter()

  const quantity = useMemo(() => {
    return productCountStore(cart, store)
  }, [store, cart, Object.keys(cart).length])

  const handleCheckout = (ev) => {
    ev.preventDefault()
    handleOpenCart(store)
  }

  return (
    <HeaderAppBar store={store.id}>
      <Toolbar>
        <Link href={store?.uri}>
          <>
            <StoreIcon src={store.logo} width={20} height={20} />
            <StoreNameContainer>
              <Typography variant="h6" style={{ flexGrow: 1 }}>
                {store?.label}
              </Typography>
            </StoreNameContainer>
          </>
        </Link>
        <SearchContainer>
          {filter && (
            <InputStyled
              placeholder="Buscar Pokemóm"
              value={filter ? filter.filter : ''}
              onChange={(ev) => filter.handleFilterPokemon(ev.target.value)}
            />
          )}
        </SearchContainer>
        <BadgeStyled
          color="secondary"
          badgeContent={quantity}
          onClick={handleCheckout}
          aria-label={
            quantity > 0
              ? `Finalizar carrinho com ${quantity} pokemon`
              : 'Carrinho vazio'
          }
        >
          <ShoppingCartIcon aria-label="Ícone para finalizar a compra" />
        </BadgeStyled>
      </Toolbar>
    </HeaderAppBar>
  )
}
