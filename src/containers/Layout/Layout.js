import Head from 'next/head'
import Link from 'next/link'
import Header from '../Header'
import Typography from '../../components/Typography'

import { allStores } from '../../../services/store'

import {
  LayoutContent,
  LayoutFooter,
  LinkStyled,
} from '../../../styles/components/Layout'

export default function Layout({ children, title, store }) {
  return (
    <>
      <Head>
        <title>Pokemon Store - {title}</title>
      </Head>
      <Header store={store} />
      <LayoutContent>{children}</LayoutContent>
      <LayoutFooter>
        {allStores.map((item) => (
          <Link key={item.id} href={item.uri}>
            <LinkStyled key={item.id} href={item.uri}>
              <img src={item.logo} width={20} height={20} alt={item.label} />
              <Typography variant="subtitle1">{item.label}</Typography>
            </LinkStyled>
          </Link>
        ))}
      </LayoutFooter>
    </>
  )
}
