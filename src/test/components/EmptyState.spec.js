import { render, screen } from '@testing-library/react'
import EmptyState from '../../components/EmptyState'

describe('EmptyState Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/DEFAULT TITLE/i)).toBeInTheDocument()
  })

  it('Should render SubTitle', () => {
    createComponent({
      subtitle: 'SUBTITLE',
    })

    expect(screen.getByText(/SUBTITLE/i)).toBeInTheDocument()
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    title: 'DEFAULT TITLE',
    ...props,
  }

  return render(<EmptyState {...defaultProps} />)
}
