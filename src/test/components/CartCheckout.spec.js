import { render, screen, fireEvent } from '@testing-library/react'
import CartCheckout from '../../components/CartCheckout'

import TestWrapper from '../Wrapper'

const dataMock = {
  store: {
    id: 'default',
    label: 'Default Store',
    uri: '/',
  },
  product: Array.from(Array(10).keys()).map((item) => ({
    id: item,
    name: `Pokemon ${item}`,
    price: (item * 2).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2,
    }),
    quantity: item === 9 ? 10 : 1,
  })),
  totalValue: '100,00',
}

describe('CartCheckout Component', () => {
  it('Should render component', () => {
    createComponent({ open: false })
    expect(screen.queryByText(/Checkout/i)).not.toBeInTheDocument()
  })

  it('Should render component with open cart', () => {
    createComponent({ open: true })
    expect(screen.getByText(/Nenhum produto no carrinho/i)).toBeInTheDocument()
  })

  it('Should render component with cart contains 10 pokemon', () => {
    createComponent({
      open: true,
      data: dataMock,
    })

    expect(
      screen.getByText(/Carrinho de compras com 10 pokemon/i)
    ).toBeInTheDocument()
    expect(screen.getByText(/Confirmar compra/i)).toBeInTheDocument()
  })

  it('Should open modal checkout successful', () => {
    const handleCheckout = jest.fn()
    createComponent({
      open: true,
      data: dataMock,
      handleCheckout,
    })

    const btnCheckout = screen.getByText(/Confirmar compra/i)
    fireEvent.click(btnCheckout)

    expect(screen.getByText(/Obrigado pela compra!/i)).toBeInTheDocument()
    expect(
      screen.getByText(
        'Você ganhou R$ 2,00 de cashback para ser usado em outras de nossas lojas.'
      )
    ).toBeInTheDocument()

    const btnFechar = screen.getByText(/Fechar/i)
    fireEvent.click(btnFechar)

    expect(handleCheckout).toBeCalled()
  })

  it('Should handle increment pokemon in the checkout', () => {
    const handleChangeQuantityProduct = jest.fn()
    createComponent({
      open: true,
      data: dataMock,
      handleChangeQuantityProduct,
    })

    const btnIncPokemon = screen.getByTitle(
      /Aumentar quantidade do produto Pokemon 8/i
    )
    fireEvent.click(btnIncPokemon)

    expect(handleChangeQuantityProduct).toBeCalled()
    expect(handleChangeQuantityProduct).toBeCalledTimes(1)

    fireEvent.click(btnIncPokemon)
    fireEvent.click(btnIncPokemon)

    expect(handleChangeQuantityProduct).toBeCalledTimes(3)
  })

  it('Should handle decrement pokemon in the checkout', () => {
    const handleChangeQuantityProduct = jest.fn()
    createComponent({
      open: true,
      data: dataMock,
      handleChangeQuantityProduct,
    })

    const btnDecPokemon = screen.getByTitle(
      /Dimínuir quantidade do produto Pokemon 9/i
    )
    fireEvent.click(btnDecPokemon)

    expect(handleChangeQuantityProduct).toBeCalled()
    expect(handleChangeQuantityProduct).toBeCalledTimes(1)

    fireEvent.click(btnDecPokemon)
    fireEvent.click(btnDecPokemon)
    fireEvent.click(btnDecPokemon)
    fireEvent.click(btnDecPokemon)

    expect(handleChangeQuantityProduct).toBeCalledTimes(5)
  })

  it('Should handle remove pokemon in the checkout', () => {
    const handleRemoveCart = jest.fn()
    createComponent({
      open: true,
      data: dataMock,
      handleRemoveCart,
    })

    const btnRemovePokemon = screen.queryAllByTitle(
      /Excluir produto Pokemon 3/i
    )
    fireEvent.click(btnRemovePokemon[1])

    expect(handleRemoveCart).toBeCalled()
    expect(handleRemoveCart).toBeCalledTimes(1)
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    data: undefined,
    open: false,
    onClose: jest.fn(),
    handleRemoveCart: jest.fn(),
    handleCheckout: jest.fn(),
    handleChangeQuantityProduct: jest.fn(),
    ...props,
  }

  return render(<CartCheckout {...defaultProps} />, {
    wrapper: TestWrapper,
  })
}
