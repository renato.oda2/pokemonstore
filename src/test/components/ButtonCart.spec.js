import { render, screen, act, fireEvent } from '@testing-library/react'
import ButtonCart from '../../components/ButtonCart'

import TestWrapper from '../Wrapper'

describe('ButtonCart Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/Comprar/i)).toBeInTheDocument()
  })

  it('Should click in the button checkout', async () => {
    const onClick = jest.fn()
    createComponent({
      product: {
        id: 25,
        name: 'Pikachu',
        quantity: 2,
        price: 10,
      },
      onClick,
    })

    const btnCheckout = screen.getByText(/Comprar/i)
    fireEvent.click(btnCheckout)

    // Existe uma animação, então para evitar de usar o jest.runOnlyPendingTimers que as vezes dá problema com animação, rodei "manual"
    await act(
      () => new Promise((resolve) => setTimeout(() => resolve(true), 1000))
    )

    expect(onClick).toHaveBeenCalled()
  })

  it('Should click in the button checkout with not function', async () => {
    createComponent({
      product: {
        id: 25,
        name: 'Pikachu',
        quantity: 2,
        price: 10,
      },
    })

    const btnCheckout = screen.getByText(/Comprar/i)
    fireEvent.click(btnCheckout)

    // Existe uma animação, então para evitar de usar o jest.runOnlyPendingTimers que as vezes dá problema com animação, rodei "manual"
    await act(
      () => new Promise((resolve) => setTimeout(() => resolve(true), 1000))
    )

    expect(screen.getByText(/Comprar/i)).toBeInTheDocument()
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    store: {
      id: 'default',
      label: 'Loja Default',
    },
    product: {},
    ...props,
  }

  return render(<ButtonCart {...defaultProps} />, {
    wrapper: TestWrapper,
  })
}
