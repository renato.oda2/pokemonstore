import { render, screen } from '@testing-library/react'
import CardPokemonSkelleton from '../../components/CardPokemonSkelleton'

import TestWrapper from '../Wrapper'

describe('CardPokemonSkelleton Component', () => {
  it('Should render component', () => {
    render(<CardPokemonSkelleton />, {
      wrapper: TestWrapper,
    })
    expect(screen.getByText(/Carregando loja.../i)).toBeInTheDocument()
  })
})
