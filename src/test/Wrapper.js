import { ThemeProvider } from '@emotion/react'
import { CartProvider } from '../hooks/Cart'

import { theme } from '../../styles/theme'

export default function TestingWrapper({ children }) {
  return (
    <ThemeProvider theme={theme}>
      <CartProvider>{children}</CartProvider>
    </ThemeProvider>
  )
}
