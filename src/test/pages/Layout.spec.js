import { render, screen, fireEvent } from '@testing-library/react'

import Layout from '../../containers/Layout'
import TestingWrapper from '../Wrapper'

describe('Layout Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/Example Store/i)).toBeInTheDocument()
  })

  it('Should click in cart', () => {
    createComponent()

    const buttonCheckout = screen.getByLabelText(/Carrinho vazio/i)
    fireEvent.click(buttonCheckout)

    expect(screen.getByText(/Nenhum produto no carrinho/i)).toBeInTheDocument()
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    store: [],
    title: 'Pikachu',
    store: { id: 'default', label: 'Example Store', uri: '/example' },
    ...props,
  }

  return render(<Layout {...defaultProps} />, {
    wrapper: TestingWrapper,
  })
}
