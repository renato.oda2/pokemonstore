import * as nextRouter from 'next/router'
import { render, screen } from '@testing-library/react'

import Product from '../../../pages/[store]/product/[id]'

import TestWrapper from '../Wrapper'

nextRouter.useRouter = jest.fn()
nextRouter.useRouter.mockImplementation(() => ({ isFallback: true }))

describe('Product fallback compoment', () => {
  it('Should render compoment', () => {
    render(<Product />, {
      wrapper: TestWrapper,
    })

    expect(screen.getByText(/Carregando loja.../i)).toBeInTheDocument()
  })
})
