import { fireEvent, render, screen } from '@testing-library/react'
import HomeScreen, { getStaticProps } from '../../../pages/index'

import { allStores } from '../../../services/store'

jest.mock('next/link', () => {
  return ({ children }) => {
    return children
  }
})

describe('HomeScreen Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/DEFAULT TITLE/i)).toBeInTheDocument()
  })

  it('Should handle click in store', () => {
    createComponent()
    const btnStore = screen.getByText(/DEFAULT TITLE/i)
    fireEvent.click(btnStore)

    expect(screen.getByText(/DEFAULT TITLE/i)).toBeInTheDocument()
  })

  it('getStaticProps', async () => {
    const props = await getStaticProps()
    expect(props).toEqual({
      props: {
        stores: allStores,
      },
    })
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    stores: [
      {
        id: 'default',
        label: 'DEFAULT TITLE',
        logo: '/assets/img/example.png',
        uri: '/default',
      },
    ],
    ...props,
  }

  return render(<HomeScreen {...defaultProps} />, {})
}
