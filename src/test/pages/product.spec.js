import * as nextRouter from 'next/router'
import { fireEvent, render, screen, act } from '@testing-library/react'
import Product, { getStaticPaths } from '../../../pages/[store]/product/[id]'

import TestWrapper from '../Wrapper'

nextRouter.useRouter = jest.fn()
nextRouter.useRouter.mockImplementation(() => ({
  route: '/',
  back: jest.fn(),
  isFallback: false,
  prefetch: jest.fn(),
}))

jest.mock('next/link', () => {
  return ({ children }) => {
    return children
  }
})

describe('Product Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/default/i)).toBeInTheDocument()
  })

  it('Should click goBack button', () => {
    createComponent()

    const btnGoBack = screen.getByLabelText(/Voltar a página/i)
    fireEvent.click(btnGoBack)

    expect(screen.getByText(/default/i)).toBeInTheDocument()
  })

  it('Should add pokemon in the cart', async () => {
    createComponent({
      product: {
        id: 1,
        name: 'Pikachu',
        price: '102,40',
      },
      store: {
        id: 'default',
        label: 'Pikachu Store',
        uri: '/',
      },
    })

    const btnAdd = screen.getByText(/Comprar/i)
    fireEvent.click(btnAdd)

    // Existe uma animação, então para evitar de usar o jest.runOnlyPendingTimers que as vezes dá problema com animação, rodei "manual"
    await act(
      () => new Promise((resolve) => setTimeout(() => resolve(true), 1000))
    )

    expect(screen.getByText(/default/i)).toBeInTheDocument()
  })

  it('getStaticPaths', async () => {
    const staticPach = await getStaticPaths()
    expect(staticPach.fallback).toBeTruthy()
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    ...props,
  }

  return render(<Product {...defaultProps} />, {
    wrapper: TestWrapper,
  })
}
