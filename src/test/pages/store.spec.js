import * as nextRouter from 'next/router'
import { render, screen } from '@testing-library/react'
import Store, {
  getStaticProps,
  getStaticPaths,
} from '../../../pages/[store]/index'

import TestWrapper from '../Wrapper'

nextRouter.useRouter = jest.fn()
nextRouter.useRouter.mockImplementation(() => ({ route: '/' }))

describe('Store Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/DEFAULT STORE/i)).toBeInTheDocument()
  })

  it('Should render Showcase store', () => {
    createComponent({
      products: [
        {
          id: 1,
          name: 'pokemon example',
          price: '129,10',
          sprites: {
            other: {
              'official-artwork': {
                front_default: '/uri/imagem.png',
              },
            },
          },
        },
      ],
    })

    expect(screen.getByText(/pokemon example/i)).toBeInTheDocument()
    expect(screen.getByText(/Listando 1 pokemon/i)).toBeInTheDocument()
  })

  it('getStaticPaths', async () => {
    const testStaticPath = await getStaticPaths()
    expect(testStaticPath).not.toBeNull()
  })

  it('getStaticProps', async () => {
    const testStaticProps = await getStaticProps({
      params: {
        store: 'dragon',
      },
    })
    expect(testStaticProps).not.toBeNull()
  })
})

const createComponent = (props = {}) => {
  const defaultProps = {
    products: [],
    store: {
      id: 'default',
      label: 'DEFAULT STORE',
      uri: '/default',
      logo: '/assets/img/logo.png',
    },
    ...props,
  }

  return render(<Store {...defaultProps} />, {
    wrapper: TestWrapper,
  })
}
