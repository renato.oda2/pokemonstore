import { render, screen } from '@testing-library/react'
import ServerError from '../../../pages/500'

describe('NotFound Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/Houve um erro interno/i)).toBeInTheDocument()
  })
})

const createComponent = () => render(<ServerError />)
