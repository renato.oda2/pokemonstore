import * as nextRouter from 'next/router'
import { render, screen } from '@testing-library/react'

import Store from '../../../pages/[store]/index'

import TestWrapper from '../Wrapper'

nextRouter.useRouter = jest.fn()
nextRouter.useRouter.mockImplementation(() => ({ isFallback: true }))

describe('Store fallback compoment', () => {
  it('Should render compoment', () => {
    render(<Store />, {
      wrapper: TestWrapper,
    })

    expect(screen.getByText(/Carregando loja.../i)).toBeInTheDocument()
  })
})
