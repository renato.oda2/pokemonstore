import { render, screen } from '@testing-library/react'
import NotFound from '../../../pages/404'

describe('NotFound Component', () => {
  it('Should render component', () => {
    createComponent()
    expect(screen.getByText(/Página não encontrada/i)).toBeInTheDocument()
  })
})

const createComponent = () => render(<NotFound />)
