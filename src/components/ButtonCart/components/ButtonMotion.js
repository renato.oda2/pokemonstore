const variantsWrapper = () => ({
  tap: {
    scaleX: 0.95,
    scaleY: 0.95,
    boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    transition: { ease: 'easeIn', duration: 0.1 },
  },
  initial: {
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    position: 'relative',
    x: 0,
    y: 0,
    scaleX: 1,
    scaleY: 1,
  },
  hover: {
    scaleX: 1.05,
    scaleY: 1.05,
    boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
  },
  active: {
    pointerEvents: 'none',
  },
  inactive: {
    pointerEvents: 'all',
  },
})

const variantsLabel = (state) => ({
  initial: { y: state ? 100 : -100 },
  exit: {
    y: state ? 100 : -100,
    transition: { ease: 'anticipate' },
  },
  in: { y: 0 },
})

const variantIcon = () => ({
  inactive: { x: 0, y: 0, scale: 1 },
  active: {
    x: 180,
    y: 0,
    scale: 2,
    transition: { ease: 'easeIn' },
  },
})

export const motionLabel = (state) => ({
  initial: 'initial',
  exit: 'exit',
  animate: 'in',
  variants: variantsLabel(state),
})

export const motionWrapper = () => ({
  initial: 'initial',
  animate: 'in',
  exit: 'out',
  whileHover: 'hover',
  whileTap: 'tap',
  variants: variantsWrapper(),
})

export const motionIconWrapper = (state) => ({
  initial: 'inactive',
  animate: state ? 'inactive' : 'active',
  variants: variantIcon(),
})
