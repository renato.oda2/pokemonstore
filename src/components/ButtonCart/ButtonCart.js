import React from 'react'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { AnimatePresence, useCycle } from 'framer-motion'
import { useCart } from '../../hooks/Cart'
import {
  ButtonContainer,
  IconWrapper,
  Label,
} from '../../../styles/components/ButtonCart'

import {
  motionWrapper,
  motionLabel,
  motionIconWrapper,
} from './components/ButtonMotion'

const labels = {
  inactive: 'Comprar',
  active: 'Comprando...',
  end: 'Pronto',
}

export default function ButtonCart({ product, store, onClick, buttonProps }) {
  const cart = useCart()
  const [state, cycleState] = useCycle('inactive', 'active')
  const initialState = 'inactive'

  const handleAddCart = (ev) => {
    ev.preventDefault()
    cycleState()
    const timeout = setTimeout(() => {
      cycleState()
      cart.handleAddCart(product, store)
      if (onClick) {
        onClick()
      }
      clearTimeout(timeout)
    }, 500)
  }

  return (
    <ButtonContainer
      {...motionWrapper(state === initialState)}
      onClick={handleAddCart}
      store={store.id}
      {...buttonProps}
    >
      <IconWrapper {...motionIconWrapper(state === initialState)}>
        <ShoppingCartIcon style={{ fill: '#000000 ' }} />
      </IconWrapper>
      <AnimatePresence>
        <Label
          variant="h6"
          key={state}
          {...motionLabel(state === initialState)}
        >
          {labels[state]}
        </Label>
      </AnimatePresence>
    </ButtonContainer>
  )
}
