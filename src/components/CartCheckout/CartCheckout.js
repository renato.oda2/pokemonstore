import { useState } from 'react'
import Slide from '@material-ui/core/Slide'

import {
  DialogCheckout,
  CartCheckoutContainer,
} from '../../../styles/components/CartCheckout'

import EmptyState from '../EmptyState'
import CardCheckoutContent from './components/CardCheckoutContent'
import CartCheckoutHeader from './components/CartCheckoutHeader'
import CartCheckoutFooter from './components/CartCheckoutFooter'
import ModalCheckout from './components/ModalCheckout'

export default function CartCheckout({
  data,
  open,
  onClose,
  handleRemoveCart,
  handleCheckout,
  handleChangeQuantityProduct,
}) {
  const [openCheckout, setOpenCheckout] = useState(false)

  const handleOpenModalCheckout = (ev) => {
    ev.preventDefault()
    setOpenCheckout(true)
  }

  const handleCloseOpenModalCheckout = () => {
    handleCheckout(data)
    setOpenCheckout(false)
  }

  const handleAddItemCart = (product) =>
    handleChangeQuantityProduct(product, data.store, 'add')

  const handleRemoveItemCart = (product) => {
    if (product.quantity > 1) {
      return handleChangeQuantityProduct(product, data.store, 'sub')
    }

    handleRemoveCart(product, data.store)
  }

  return (
    <>
      <DialogCheckout
        open={open}
        onClose={onClose}
        TransitionComponent={Slide}
        TransitionProps={{
          mountOnEnter: true,
          direction: 'right',
        }}
        disableBackdropClick={true}
        hideBackdrop={false}
        fullScreen={false}
        disableScrollLock={false}
      >
        <CartCheckoutContainer role="document">
          <CartCheckoutHeader data={data} onClose={onClose} />
          {data && data?.product?.length > 0 ? (
            <CardCheckoutContent
              data={data}
              handleRemoveItemCart={handleRemoveItemCart}
              handleAddItemCart={handleAddItemCart}
              handleRemoveCart={handleRemoveCart}
            />
          ) : (
            <EmptyState title="Nenhum produto no carrinho" />
          )}
          {data && data?.product?.length > 0 && (
            <CartCheckoutFooter
              data={data}
              handleCheckout={handleOpenModalCheckout}
              totalValue={data.totalValue}
            />
          )}
        </CartCheckoutContainer>
      </DialogCheckout>
      <ModalCheckout
        open={openCheckout}
        onClose={handleCloseOpenModalCheckout}
        store={data?.store}
        totalValue={data ? data.totalValue : 'R$ 0,00'}
      />
    </>
  )
}
