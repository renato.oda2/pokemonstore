import Typography from '@material-ui/core/Typography'
import Close from '@material-ui/icons/Close'
import {
  CartCheckoutHeaderStyled,
  CartCheckoutButtonClose,
} from '../../../../styles/components/CartCheckout'

export default function CartCheckoutHeader({ onClose, data }) {
  return (
    <CartCheckoutHeaderStyled>
      <CartCheckoutButtonClose onClick={onClose}>
        <Close />
      </CartCheckoutButtonClose>
      {data?.store && (
        <Typography variant="h6" noWrap>
          Checkout - {data?.store?.label}
        </Typography>
      )}
    </CartCheckoutHeaderStyled>
  )
}
