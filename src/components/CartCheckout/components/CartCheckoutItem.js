import { useMemo } from 'react'
import Tooltip from '@material-ui/core/Tooltip'
import Grid from '@material-ui/core/Grid'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import {
  CheckoutItem,
  CheckoutItemImage,
  CheckoutItemProductDetails,
  CheckoutItemProductTotalValue,
  CheckoutItemProductRemove,
  ButtonCheckout,
} from '../../../../styles/components/CartCheckout'

import Typography from '../../Typography'

export default function CartCheckoutItem({
  data,
  store,
  handleRemoveItemCart,
  handleAddItemCart,
  handleRemoveCart,
}) {
  const totalValue = useMemo(
    () =>
      (Number(data.price.replace(/,/g, '.')) * data.quantity).toLocaleString(
        'pt-BR',
        {
          currency: 'BRL',
          minimumFractionDigits: 2,
        }
      ),
    [data.quantity, data.price]
  )

  return (
    <CheckoutItem
      store={store.id}
      aria-label={`Informações do produto ${data.name}`}
    >
      <div className="product-image">
        <CheckoutItemImage
          src={data?.sprites?.other['official-artwork']?.front_default}
          width={150}
          height={150}
        />
      </div>
      <CheckoutItemProductDetails>
        <Typography variant="caption">{data.name}</Typography>
        <Typography variant="subtitle1">R$ {data.price}</Typography>
        <Grid container spacing={0}>
          <Grid item>
            <Tooltip
              title={
                data.quantity > 1
                  ? `Dimínuir quantidade do produto ${data.name}`
                  : `Excluir produto ${data.name}`
              }
            >
              <ButtonCheckout onClick={() => handleRemoveItemCart(data)}>
                <ExpandMoreIcon />
              </ButtonCheckout>
            </Tooltip>
          </Grid>
          <Grid item>
            <Typography variant="subtitle2">Un {data.quantity}</Typography>
          </Grid>
          <Grid item>
            <Tooltip title={`Aumentar quantidade do produto ${data.name}`}>
              <ButtonCheckout onClick={() => handleAddItemCart(data)}>
                <ExpandLessIcon />
              </ButtonCheckout>
            </Tooltip>
          </Grid>
        </Grid>
      </CheckoutItemProductDetails>
      <CheckoutItemProductTotalValue>
        <Typography variant="subtitle1">R$ {totalValue}</Typography>
      </CheckoutItemProductTotalValue>
      <Tooltip title={`Excluir produto ${data.name}`}>
        <CheckoutItemProductRemove
          aria-label={`Excluir produto ${data.name}`}
          onClick={() => handleRemoveCart(data, store)}
        >
          <DeleteForeverIcon style={{ fill: '#ff0000' }} />
        </CheckoutItemProductRemove>
      </Tooltip>
    </CheckoutItem>
  )
}
