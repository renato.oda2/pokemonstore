import Typography from '@material-ui/core/Typography'
import {
  CartCheckoutFooterStyled,
  CartCheckoutFooterCompleted,
  CartButtonCheckout,
} from '../../../../styles/components/CartCheckout'

export default function CartCheckoutFooter({
  data,
  totalValue,
  handleCheckout,
}) {
  return (
    <CartCheckoutFooterStyled
      container
      wrap="nowrap"
      alignItems="center"
      role="footer"
    >
      <Typography variant="subtitle2">Valor Total R$ {totalValue}</Typography>
      <CartCheckoutFooterCompleted
        container
        xs
        alignItems="center"
        justify="flex-end"
      >
        <CartButtonCheckout store={data.store.id} onClick={handleCheckout}>
          Confirmar compra
        </CartButtonCheckout>
      </CartCheckoutFooterCompleted>
    </CartCheckoutFooterStyled>
  )
}
