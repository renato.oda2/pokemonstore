import { useMemo } from 'react'

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CartCheckoutHeader from './CartCheckoutHeader'
import { ButtonCheckout } from '../../../../styles/components/CartCheckout'

export default function ModalCheckout({ open, onClose, store, totalValue }) {
  const cashbackValue = useMemo(() => {
    if (!totalValue) {
      return '0.00'
    }

    const value = Number(totalValue.replace(/\./g, '').replace(/,/g, '.'))

    return (value * 0.02).toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2,
    })
  }, [totalValue])

  return (
    <Dialog open={open}>
      <CartCheckoutHeader onClose={onClose} data={{ store }} />
      <DialogContent>
        <Grid
          container
          spacing={2}
          justify="center"
          alignItems="center"
          alignContent="center"
        >
          <Grid item xs={12} sm={2}>
            <img src={store?.logo} alt={store?.label} width={80} height={80} />
          </Grid>
          <Grid item xs={10} sm={10}>
            <Typography variant="h6">Obrigado pela compra!</Typography>
            <Typography variant="caption">
              Você ganhou R$ {cashbackValue} de cashback para ser usado em
              outras de nossas lojas.
            </Typography>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <ButtonCheckout onClick={onClose}>Fechar</ButtonCheckout>
      </DialogActions>
    </Dialog>
  )
}
