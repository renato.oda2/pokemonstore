import Typography from '../../Typography'
import {
  CartCheckoutContentStyled,
  CheckoutItemContent,
} from '../../../../styles/components/CartCheckout'

import CartCheckoutItem from './CartCheckoutItem'

export default function CardCheckoutContent({ data, ...props }) {
  return (
    <CartCheckoutContentStyled>
      <Typography variant="srOnly">
        Carrinho de compras com {data.product.length} pokemon
      </Typography>
      <CheckoutItemContent>
        {data.product.map((item) => (
          <CartCheckoutItem
            key={item.name}
            data={item}
            store={data.store}
            {...props}
          />
        ))}
      </CheckoutItemContent>
    </CartCheckoutContentStyled>
  )
}
