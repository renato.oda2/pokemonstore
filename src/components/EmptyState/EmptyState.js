import {
  Container,
  Content,
  IconContainer,
} from '../../../styles/components/EmptyState'

import Typography from '../Typography'

export default function EmptyState({ title, subtitle }) {
  return (
    <Container>
      <Content>
        <IconContainer>
          <img src="/assets/images/pikachu.webp" />
        </IconContainer>
        <Typography variant="h6">{title}</Typography>
        {subtitle && <Typography variant="caption">{subtitle}</Typography>}
      </Content>
    </Container>
  )
}
