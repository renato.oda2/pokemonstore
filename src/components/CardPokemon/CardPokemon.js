import Link from 'next/link'
import Tooltip from '@material-ui/core/Tooltip'
import ButtonCart from '../ButtonCart'
import {
  Container,
  Content,
  ProductImage,
  ProductCheckout,
  TextProduct,
  TextPriceProduct,
} from '../../../styles/components/CardPokemon'

export default function CardPokemon({ product, store }) {
  return (
    <Container>
      <Link href={`${store.uri}/product/${product.id}`}>
        <Content
          store={store.id}
          aria-label={`Informações do produto ${product.name}`}
        >
          <ProductImage
            src={product?.sprites?.other['official-artwork']?.front_default}
            width="250"
            height="250"
            alt={product.name}
          />
        </Content>
      </Link>
      <ProductCheckout>
        <TextPriceProduct variant="h6">R$ {product.price}</TextPriceProduct>
        <Tooltip title={product.name}>
          <TextProduct variant="caption" noWrap>
            {product.name}
          </TextProduct>
        </Tooltip>
        <ButtonCart
          product={product}
          store={store}
          disabled={true}
          buttonProps={{ 'aria-label': `Comprar pokemon ${product.name}` }}
        >
          Comprar
        </ButtonCart>
      </ProductCheckout>
    </Container>
  )
}
