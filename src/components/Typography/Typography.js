import { TypographyStyled } from '../../../styles/components/Typography'

export default function Typography({ transform = 'capitalize', ...props }) {
  return <TypographyStyled transform={transform} {...props} />
}
