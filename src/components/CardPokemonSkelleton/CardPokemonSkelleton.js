import Skeleton from 'react-loading-skeleton'
import Grid from '@material-ui/core/Grid'
import Typography from '../Typography'
import Layout from '../../containers/Layout'

export default function CardPokemonSkelleton() {
  return (
    <Layout store={{ id: 'default', label: 'Carregando', uri: '/' }}>
      <Typography variant="srOnly">Carregando loja...</Typography>
      <Grid container spacing={3}>
        {Array.from(Array(20).keys()).map((item) => (
          <Grid item key={item} xs={12} sm={6} md={4} lg={2}>
            <Skeleton count={3} />
          </Grid>
        ))}
      </Grid>
    </Layout>
  )
}
