import { useState, useEffect } from 'react'

import CartContext from './CartContext'
import CartCheckout from '../../components/CartCheckout'

export default function CartProvider({ children }) {
  const [cart, setCart] = useState({})
  const [openCart, setOpenCart] = useState(false)
  const [dataCart, setDataCart] = useState(undefined)

  useEffect(() => {
    const cartStorage = localStorage.getItem('cart') || {}
    if (Object.keys(cartStorage).length > 0) {
      setCart(JSON.parse(cartStorage))
    }
  }, [])

  useEffect(() => {
    if (Object.keys(cart).length > 0) {
      localStorage.setItem('cart', JSON.stringify(cart))
    } else {
      localStorage.removeItem('cart')
    }
  }, [cart, Object.keys(cart).length])

  const productValueStore = (product) => {
    if (product?.length === 0) {
      return 0
    }

    let value = 0
    product.forEach(
      (item) => (value += Number(item.price.replace(/,/g, '.')) * item.quantity)
    )
    return value
  }

  const handleAddCartData = (cartStore) =>
    setDataCart(() => {
      if (!cartStore) {
        return undefined
      }

      const value = productValueStore(cartStore.product)
      return {
        ...cartStore,
        totalValue: value.toLocaleString('pt-BR', {
          currency: 'BRL',
          minimumFractionDigits: 2,
        }),
      }
    })

  const handleAddCart = (product, store) => {
    setCart((state) => {
      const cartStore = Object.keys(state).find((item) => item === store.id)
      if (!cartStore) {
        return {
          ...cart,
          [store.id]: {
            store,
            product: [{ ...product, quantity: 1 }],
          },
        }
      }

      const productStore = state[cartStore].product.findIndex(
        (item) => item.id === product.id
      )
      let newProducts = state[cartStore].product
      if (productStore >= 0) {
        newProducts[productStore].quantity =
          newProducts[productStore].quantity + 1
      } else {
        newProducts.push({ ...product, quantity: 1 })
      }

      return {
        ...cart,
        [store.id]: {
          store,
          product: newProducts,
        },
      }
    })
  }

  const handleRemoveCart = (product, store) => {
    setCart((state) => {
      const cartStore = Object.keys(state).find((item) => item === store.id)

      const productStore = state[cartStore].product.filter(
        (item) => item.id !== product.id
      )

      if (dataCart?.store?.id === store.id) {
        handleAddCartData({ store, product: productStore })
      }

      return {
        ...cart,
        [store.id]: {
          store,
          product: productStore,
        },
      }
    })
  }

  const handleOpenCart = (store) => {
    const cartStore = Object.keys(cart).find((item) => item === store.id)
    handleAddCartData(cart[cartStore])
    setOpenCart(true)
  }

  const handleCloseCart = () => setOpenCart(false)

  const handleCheckout = (data) =>
    setCart((state) => {
      let newState = state
      delete newState[data.store.id]

      handleCloseCart()
      handleAddCartData(undefined)

      return newState
    })

  const productCountStore = (cartStorage, store) => {
    const cartStore = Object.keys(cartStorage).find((item) => item === store.id)
    if (!cartStore) {
      return 0
    }

    let quantity = 0
    cartStorage[cartStore].product.forEach(
      (item) => (quantity += item.quantity)
    )
    return quantity
  }

  const handleChangeQuantityProduct = (product, store, type) => {
    setCart((state) => {
      const cartStore = Object.keys(state).find((item) => item === store.id)

      const productStore = state[cartStore].product.findIndex(
        (item) => item.id === product.id
      )

      let newProducts = state[cartStore].product
      newProducts[productStore].quantity =
        type === 'add'
          ? newProducts[productStore].quantity + 1
          : newProducts[productStore].quantity - 1

      handleAddCartData({
        store,
        product: newProducts,
      })

      return {
        ...cart,
        [store.id]: {
          store,
          product: newProducts,
        },
      }
    })
  }

  const propsProvider = {
    cart,
    handleAddCart,
    handleRemoveCart,
    handleOpenCart,
    handleCloseCart,
    productCountStore,
  }

  return (
    <CartContext.Provider value={propsProvider}>
      {children}
      {openCart && (
        <CartCheckout
          data={dataCart}
          open={openCart}
          onClose={handleCloseCart}
          handleRemoveCart={handleRemoveCart}
          handleCheckout={handleCheckout}
          handleChangeQuantityProduct={handleChangeQuantityProduct}
        />
      )}
    </CartContext.Provider>
  )
}
