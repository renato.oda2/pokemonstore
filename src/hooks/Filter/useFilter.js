import { useContext } from 'react'
import FilterContext from './FilterContext'

export default function useFilter() {
  return useContext(FilterContext)
}
