import { useRouter } from 'next/router'
import { useState, useMemo, useEffect } from 'react'

import FilterContext from './FilterContext'

export default function FilterProvider({ children, data }) {
  const [filterText, setFilterText] = useState('')
  const { asPath } = useRouter()

  useEffect(() => setFilterText(''), [asPath])

  const filterPokemon = useMemo(() => {
    if (filterText) {
      return data.filter((item) => item.name.includes(filterText))
    }
    return data
  }, [data, data?.pokemon, filterText])

  const handleFilterPokemon = (value) => setFilterText(value.toLowerCase())

  return (
    <FilterContext.Provider value={{ filter: filterText, handleFilterPokemon }}>
      {children({ pokemon: data, filterPokemon })}
    </FilterContext.Provider>
  )
}
