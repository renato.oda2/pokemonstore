import { createContext } from 'react'

export const FilterContext = createContext()

export default FilterContext
