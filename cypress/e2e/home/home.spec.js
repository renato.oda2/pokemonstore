/// <reference types="cypress" />

context('Home', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Should render Page Home', () => {
    cy.contains('Dragon Store')
    cy.contains('Water Store')
    cy.contains('Fire Store')
    cy.contains('Psychic Store')
  })

  it('Should onClick in Dragon Store page', () => {
    cy.contains('Dragon Store').click()
    cy.location({ timeout: 50000 }).should((loc) => {
      expect(loc.pathname).to.eq('/dragon')
    })
  })

  it('Should onClick in Water Store page', () => {
    cy.contains('Water Store').click()
    cy.location({ timeout: 50000 }).should((loc) => {
      expect(loc.pathname).to.eq('/water')
    })
  })

  it('Should onClick in Fire Store page', () => {
    cy.contains('Fire Store').click()
    cy.location({ timeout: 50000 }).should((loc) => {
      expect(loc.pathname).to.eq('/fire')
    })
  })

  it('Should onClick in Psychic Store page', () => {
    cy.contains('Psychic Store').click()
    cy.location({ timeout: 50000 }).should((loc) => {
      expect(loc.pathname).to.eq('/psychic')
    })
  })
})
