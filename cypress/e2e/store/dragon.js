/// <reference types="cypress" />

context('Home', () => {
  beforeEach(() => {
    cy.visit('/dragon', { timeout: 100000 })
  })

  it('Should process checkout in store', () => {
    cy.get('[aria-label="Comprar pokemon dratini"]').click()
    cy.get('[aria-label="Comprar pokemon kingdra"]').click()

    cy.get('[aria-label="Finalizar carrinho com 2 pokemon"]').click()

    expect(cy.contains('Valor Total R$ 165,00')).not.null

    cy.get('[aria-label="Excluir produto dratini"]').click()

    expect(cy.contains('Valor Total R$ 135,00')).not.null

    cy.get('button').contains('Confirmar compra').click()
    cy.contains('Obrigado pela compra!').should('not.be.null')
    cy.contains(
      'Você ganhou R$ 2,70 de cashback para ser usado em outras de nossas lojas'
    ).should('not.be.null')
  })

  it('Should view Product details in the store', () => {
    cy.get('[aria-label="Informações do produto dratini"]').click()

    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/dragon/product/147')
    })

    cy.get('[aria-label="Voltar a página"]').click()

    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/dragon')
    })
  })

  it('Should filter pokemon', () => {
    cy.get('input').type('Flygon')
    cy.contains('Listagem de pokemon 1 de 78').should('be.ok')
  })

  it('Should filter parcial Pokemon', () => {
    cy.get('input').type('Drag')
    cy.contains('Listagem de pokemon 5 de 78').should('be.ok')
  })
})
