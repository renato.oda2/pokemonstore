const withPlugins = require('next-compose-plugins')
const optimizedImages = require('next-optimized-images')

module.exports = withPlugins(
  [
    [
      optimizedImages,
      {
        inlineImageLimit: 8192,
        imagesFolder: 'images',
        imagesName: '[name]-[hash].[ext]',
        handleImages: ['jpeg', 'png', 'svg', 'webp', 'gif'],
        removeOriginalExtension: false,
        optimizeImages: true,
        optimizeImagesInDev: false,
        optipng: {
          optimizationLevel: 3,
        },
        webp: {
          preset: 'default',
          quality: 75,
        },
      },
    ],
  ],
  {
    images: {
      domains: ['raw.githubusercontent.com'],
    },
    strictMode: true,
  }
)
