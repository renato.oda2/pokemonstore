
# Pokemon Store

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/renato.oda2/pokemonstore/main)](https://gitlab.com/renato.oda2/pokemonstore/-/pipelines)
[![Gitlab code coverage](https://img.shields.io/gitlab/coverage/renato.oda2/pokemonstore/main)](https://renato.oda2.gitlab.io/pokemonstore/)
[![Cypress Dashboard](https://img.shields.io/badge/Cypress-Dashboard-green)](https://dashboard.cypress.io/projects/droawn)

Challenge of a store that consists of selling pokémon by category of species.

This challenge includes four stores with a single source code just validating which type of store open by url.

### Demo
[![Vercel](https://therealsujitk-vercel-badge.vercel.app/?app=pokemonstore-desafio)](https://pokemonstore-desafio.vercel.app/)

The project is published on the Vercel platform with the following link: https://pokemonstore-desafio.vercel.app/water

### Print

### Desktop
<img src="/docs/Home.PNG" alt="PrintScreen from Home Page in Desktop" weight={400} width={400} />
<img src="/docs/Showcase.PNG" alt="PrintScreen from ShowCase Page in Desktop" weight={400} width={400} />
<img src="/docs/Product.PNG" alt="PrintScreen from Product Page in Desktop" weight={400} width={400} />

### Mobile
<img src="/docs/MobileHome.jpg" alt="PrintScreen from Home Page in Mobile App" weight={400} width={400} />
<img src="/docs/MobileShowCase.jpg" alt="PrintScreen from ShowCase Page in Mobile App" weight={400} width={400} />
<img src="/docs/MobileCheckout.jpg" alt="PrintScreen from Checkout Page in Mobile App" weight={400} width={400} />


## Start project

This project use [pnpm](https://pnpm.io/) for  package manager with node.

```bash
$ pnpm install
$ pnpm dev
```

Open the browser on the page http://localhost:3000

### Features
- Four store selling Pokemon
- Showcase Store
- Product Details
- Checkout
- Mobile First
- Responsive
- Local cache with PWA

### Technology

- React
- Next.JS
- Styled Component with Emotion
- Material-UI
- Progessive Web App (PWA)
- Incremental Static regeneration (ISR) with Next.JS
- Testing End To End (E2E) with Cypress
- Integration and Unit Testing with Testing Library
- EsLint
- Prettier
- Babel.JS
- GitLab CI/CD
- Github CI
- CircleCI CI
- Code Quality with SonarQube (Hosted with SonarCloud)
- Snyk.IO dependency scan (SAST)
- Package Manager with pnpm
- [Todo] - Helm Chart Template
- [Todo] - Container Scanning

### Lighthouse

Validation page Home in 14/04/2021 12:18
[Home Lighthouse report](https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https://pokemonstore-desafio.vercel.app)
<img src="/docs/lighthouse-home.png" alt="Image validation page Home in lighthouse online" weight={400} width={400} />

Validation page Dragon in 14/04/2021 12:19
[Dragon Lighthouse report](https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https://pokemonstore-desafio.vercel.app/dragon)
<img src="/docs/lighthouse-dragon.png" alt="Image validation page dragon in lighthouse online" weight={400} width={400} />

## Docker

[![Docker Image Size (tag)](https://img.shields.io/docker/image-size/oda2/pokemon-store/latest)](https://hub.docker.com/r/oda2/pokemon-store)

### Run image docker local

To run the latest published version you can run the following command

```bash
$ docker run - p 3000:3000 --name pokemon-store oda2/pokemon-store
```

### Links

- Project Deployed https://pokemonstore-desafio.vercel.app
- PNPM https://pnpm.io/
- Dashboard Cypress https://dashboard.cypress.io/projects/droawn
- Docker Image https://hub.docker.com/r/oda2/pokemon-store
- Report lighthouse online Home Page https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https://pokemonstore-desafio.vercel.app
- Report lighthouse online ShowCase Page https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https://pokemonstore-desafio.vercel.app/dragon
- SonarQube https://sonarcloud.io/dashboard?id=renato.oda2_pokemonstore
- Code Coverage publish https://renato.oda2.gitlab.io/pokemonstore/

## License

Licensed under [MIT](/LICENSE)
