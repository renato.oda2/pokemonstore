import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Grid from '@material-ui/core/Grid'
import Layout from '../../src/containers/Layout'
import CardPokemon from '../../src/components/CardPokemon'
import Typography from '../../src/components/Typography'
import CardPokemonSkelleton from '../../src/components/CardPokemonSkelleton'
import { fetchPokemonByType, fetchPokemonByUrl } from '../../services/pokemon'
import { getStore } from '../../services/store'

import { FilterProvider } from '../../src/hooks/Filter'

export default function Home({ store, products }) {
  const [mounted, setMounted] = useState(false)
  const { isFallback } = useRouter()

  useEffect(() => setMounted(true), [])
  if (!mounted) return null

  if (isFallback) {
    return <CardPokemonSkelleton />
  }

  return (
    <FilterProvider data={products}>
      {({ filterPokemon }) => {
        return (
          <Layout title={store?.label} store={store}>
            <Grid container spacing={4}>
              <Typography variant="srOnly">
                Listando {filterPokemon.length} pokemon
              </Typography>
              {filterPokemon.map((item) => (
                <Grid item key={item.name} xs={12} sm={6} md={4} lg={2}>
                  <CardPokemon product={item} store={store} />
                </Grid>
              ))}
            </Grid>
          </Layout>
        )
      }}
    </FilterProvider>
  )
}

export async function getStaticPaths() {
  return {
    paths: ['/water', '/fire', '/dragon', '/psychic', '/normal'],
    fallback: true,
  }
}

export async function getStaticProps(context) {
  const res = await fetchPokemonByType(context.params.store)
  if (res.status !== 200) {
    return { notFound: true }
  }

  const products = await Promise.all(
    res.data.map(async (item) => {
      const details = await fetchPokemonByUrl(item.pokemon.url)
      return {
        name: item.pokemon.name,
        ...details.data,
      }
    })
  )

  const store = getStore(context.params.store)

  return {
    props: {
      store: store,
      count: products.length - 1,
      products: products,
      next: false,
      previous: false,
    },
    // revalidate: 72000 //20h
  }
}
