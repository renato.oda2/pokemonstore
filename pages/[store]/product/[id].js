import { useRouter } from 'next/router'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Grid from '@material-ui/core/Grid'
import Layout from '../../../src/containers/Layout'
import { fetchPokemonById } from '../../../services/pokemon'
import { getStore } from '../../../services/store'

import Typography from '../../../src/components/Typography'
import CardPokemonSkelleton from '../../../src/components/CardPokemonSkelleton'

import {
  Container,
  ContainerWrapper,
  ProductDetails,
  ProductLeft,
  ProductImageContainer,
  ProductRight,
  ProductFooter,
  ProductInfo,
  ButtonGoBack,
} from '../../../styles/pages/Product'

import ButtonCart from '../../../src/components/ButtonCart'

export default function Product({
  product,
  store = { id: 'default', uri: '/' },
}) {
  const router = useRouter()

  if (router.isFallback) {
    return <CardPokemonSkelleton />
  }

  return (
    <Layout
      title={`Pokemon Store - Loja do Dragão -${product?.name}`}
      store={store}
    >
      <Container>
        <ContainerWrapper>
          <ButtonGoBack
            aria-label="Voltar a página"
            onClick={() => router.back()}
            store={store.id}
          >
            <ArrowBackIcon aria-label="Icone de voltar" />
          </ButtonGoBack>
          <ProductDetails>
            <ProductLeft>
              <ProductInfo>
                <Typography variant="h3">{product?.name}</Typography>
                <Typography variant="subtitle1">{store.id}</Typography>
              </ProductInfo>
              <ProductImageContainer>
                <img
                  src={
                    product?.sprites?.other['official-artwork']?.front_default
                  }
                  width={150}
                  height={150}
                />
              </ProductImageContainer>
            </ProductLeft>
            <ProductRight>
              <Typography variant="caption">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Mattis vulputate enim nulla aliquet porttitor lacus. Maecenas
                ultricies mi eget mauris. Sed adipiscing diam donec adipiscing
                tristique risus nec feugiat in. Fringilla urna porttitor rhoncus
                dolor purus non. Cursus metus aliquam eleifend mi in nulla.
              </Typography>
            </ProductRight>
            <ProductFooter>
              <Grid container spacing={0}>
                <Grid item xs={4} lg={8}>
                  <Typography variant="subtitle1">Preço Total</Typography>
                  <Typography variant="subtitle2">
                    R$ {product?.price}
                  </Typography>
                </Grid>
                <Grid item xs={8} lg={4} container justify="flex-end">
                  <ButtonCart
                    store={store}
                    product={product}
                    onClick={() => router.back()}
                  />
                </Grid>
              </Grid>
            </ProductFooter>
          </ProductDetails>
        </ContainerWrapper>
      </Container>
    </Layout>
  )
}

export async function getStaticPaths() {
  return {
    paths: [
      '/water/product/6',
      '/fire/product/3',
      '/dragon/product/147',
      '/psychic/product/36',
    ],
    fallback: true,
  }
}

export async function getStaticProps(context) {
  const res = await fetchPokemonById(context.params.id)

  return {
    props: {
      store: getStore(context.params.store),
      product: res.data,
    },
  }
}
