/* eslint no-console: "off" */
import { useEffect } from 'react'
import { ThemeProvider, CacheProvider } from '@emotion/react'
import '../styles/globals.css'
import { theme } from '../styles/theme'

import { CartProvider } from '../src/hooks/Cart'
import { cache } from '../styles/cache'

export default function App({ Component, pageProps }) {
  useEffect(() => {
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function load() {
        navigator.serviceWorker
          .register('/service-worker.js')
          .then((registration) => {
            console.log(
              'Service Worker successfull registration. Scope: ',
              registration.scope
            )
          })
          .catch((err) =>
            console.log('Service Worker: Error registration ', err)
          )
      })
    }
  }, [])

  return (
    <CacheProvider value={cache}>
      <ThemeProvider theme={theme}>
        <CartProvider>
          <Component {...pageProps} />
        </CartProvider>
      </ThemeProvider>
    </CacheProvider>
  )
}
