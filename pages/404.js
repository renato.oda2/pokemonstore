import EmptyState from '../src/components/EmptyState'

export default function NotFoundPage() {
  return (
    <EmptyState
      title="Página não encontrada"
      subtitle="A página solicitada não foi encontrada"
    />
  )
}
