import Link from 'next/link'
import Head from 'next/head'
import Typography from '@material-ui/core/Typography'

import { allStores } from '../services/store'

import { Container, ContainerTitle } from '../styles/pages/Home'

export default function Home({ stores }) {
  return (
    <>
      <Head>
        <title>Pokemon Store</title>
      </Head>
      <Container>
        {stores.map((item) => (
          <Link key={item.uri} href={item.uri}>
            <ContainerTitle backgroundColor={item.background}>
              <img
                src={item.logo}
                alt={`Logo da loja do pokemon ${item.label}`}
                width={120}
                height={120}
              />
              <Typography variant="h6">{item.label}</Typography>
            </ContainerTitle>
          </Link>
        ))}
      </Container>
    </>
  )
}

export async function getStaticProps() {
  return {
    props: { stores: allStores },
  }
}
