import EmptyState from '../src/components/EmptyState'

export default function NotFoundPage() {
  return (
    <EmptyState
      title="Houve um erro interno"
      subtitle="A página solicitada não foi processada"
    />
  )
}
