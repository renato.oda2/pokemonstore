import React from 'react'
import { ServerStyleSheets } from '@material-ui/core/styles'
import Document, { Html, Head, Main, NextScript } from 'next/document'
import createEmotionServer from '@emotion/server/create-instance'
import { cache } from '../styles/cache'

const { extractCritical } = createEmotionServer(cache)

export default class DocumentPokemonStore extends Document {
  render() {
    return (
      <Html lang="pt">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="description" content="Loja de venda de Pokemon" />
          <meta name="keywords" content="Pokemon Store" />
          <meta property="og:title" content="Pokemon Store"></meta>
          <meta property="og:site_name" content="Pokemon Store"></meta>
          <meta
            property="og:url"
            content="https://pokemonstore-eight.vercel.app/"
          ></meta>
          <meta
            property="description"
            content="Loja de venda de Pokemon"
          ></meta>
          <meta property="og:type" content="website"></meta>
          <link rel="manifest" href="/manifest.json" />
        </Head>
        <body>
          <Main />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <NextScript />
        </body>
      </Html>
    )
  }
}

DocumentPokemonStore.getInitialProps = async (ctx) => {
  const sheets = new ServerStyleSheets()
  const originalRenderPage = ctx.renderPage

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
    })

  const initialProps = await Document.getInitialProps(ctx)
  const styles = extractCritical(initialProps.html)

  return {
    ...initialProps,
    styles: [
      ...React.Children.toArray(initialProps.styles),
      sheets.getStyleElement(),
      <style
        key="emotion-style-tag"
        data-emotion={`css ${styles.ids.join(' ')}`}
        dangerouslySetInnerHTML={{ __html: styles.css }}
      />,
    ],
  }
}
