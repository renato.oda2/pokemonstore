module.exports = {
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: ['@testing-library/jest-dom'],
  collectCoverage: true,
  coverageDirectory: 'coverage',
  collectCoverageFrom: [
    '<rootDir>/pages/**/*.js',
    '<rootDir>/src/**/*.js',
    '<rootDir>/styles/**/*.js',
    '!<rootDir>/src/test/**/*',
    '!<rootDir>/pages/_app.js',
    '!<rootDir>/pages/_document.js',
    '!<rootDir>/styles/cache.js',
    '!<rootDir>/pages/api/**/*.js',
  ],
  testPathIgnorePatterns: [
    '/.next/',
    '/node_modules/',
    '/coverage/',
    '/cypress',
  ],
}
