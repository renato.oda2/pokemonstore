/* eslint no-console: "off" */
let cacheName = 'pokemon-store-v.1'
let filesToCache = [
  './',
  'index.html',
  '/assets/icon/128.png',
  '/assets/icon/512.png',
  '/dragon.html',
  '/water.html',
  '/fire.html',
  '/psychic.html',
]

self.addEventListener('install', function (e) {
  console.log('[ServiceWorker] Installer');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function (e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
});

self.addEventListener('fetch', function (e) {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});
